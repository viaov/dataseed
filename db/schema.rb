# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130715090856) do

  create_table "colors", force: true do |t|
    t.string   "name"
    t.string   "hex"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "datasets", force: true do |t|
    t.string   "name"
    t.string   "guid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fields", force: true do |t|
    t.string   "name"
    t.string   "field_type"
    t.string   "field_options"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "dataset_id",    limit: 128
    t.string   "template"
  end

  create_table "first_names", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "gender",     limit: 1
  end

  create_table "last_names", force: true do |t|
    t.string   "name"
    t.float    "pct_white"
    t.float    "pct_asian"
    t.float    "pct_black"
    t.float    "pct_hispanic"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "zip_codes", force: true do |t|
    t.string   "zip"
    t.string   "state"
    t.string   "city"
    t.integer  "area_code"
    t.float    "lat"
    t.float    "long"
    t.string   "country"
    t.string   "zip_type"
    t.string   "county"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
