class CreateZipCodes < ActiveRecord::Migration
  def change
    create_table :zip_codes do |t|
      t.string :zip
      t.string :state
      t.string :city
      t.float :lat
      t.float :long
      t.string :county
      t.timestamps
    end
  end
end
