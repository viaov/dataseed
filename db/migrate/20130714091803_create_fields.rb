class CreateFields < ActiveRecord::Migration
  def change
    create_table :fields do |t|
      t.string :name
      t.string :field_type
      t.string :field_options
	  t.integer :dataset_id
	  t.string :template
      t.timestamps
    end
  end
end
