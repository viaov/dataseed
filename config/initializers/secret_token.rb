# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Dataseed::Application.config.secret_key_base = '86fbdfe80636fb9bab18a30f4db12b873bacd235fd68561746e192abfd6c43843d140ac80f2a2315c79af60f090e649e9493168373f73b6567100851d439a5e4'
