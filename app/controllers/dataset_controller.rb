require 'json'
class DatasetController < ApplicationController
	def new
	end
	def index
		@datasets = Dataset.all
	end
	def create
		@dataset = Dataset.new(params.require(:dataset).permit(:name))
		@dataset.guid = SecureRandom.uuid()
		@dataset.save
		redirect_to '/d/' + @dataset.guid
	end
	def show
		guid = params[:guid]
		@dataset = Dataset.where('guid=?', guid).first
	end
	def newfield
		guid = params[:guid]
		@dataset = Dataset.where('guid=?', guid).first
		@dataset.fields.create(name: params[:field][:name], template: params[:field][:template], field_options: params[:option].to_json)
		redirect_to "/d/#{params[:guid]}"
	end
	def delete_field
		id = params[:fieldid]
		Field.find(id).destroy()
		redirect_to "/d/#{params[:guid]}"
	end
	def generate
		startTime = Time.new
		guid = params[:guid]
		@dataset = Dataset.where('guid=?', guid).first
		@data = generate_dataset(@dataset)
		endTime = Time.new
		@processing_time = endTime - startTime
	end


	def generate_dataset(dataset, amount=10)
		dataList = []
		@generation_count = -1
		amount.times {
			@generation_count = @generation_count + 1
			data = {}
			constraints = {}
			dataset.fields.each do |field|
				if field.field_options != 'null' then
					fo = JSON.parse(field.field_options)
					if fo['constraint'] != nil  && fo['constraint'] != 'none' then
						if data[fo['constraint']] != nil then
							data[field.name] = generate_field(field,data[fo['constraint']])
							next
						else
							constraints[fo['constraint']] = field
							next
						end
					end
				end
				data[field.name] = generate_field(field)

				constraints = constraints.select{|k,v| k=field.name}
				constraints.each do |k, v|
					data[v.name] = generate_field(v, data[k])
				end


			end
			dataList.push(data)
		}
		return dataList
	end
	def is_string?(fieldName)
		@dataset.fields.each do |field|
			if field.name == fieldName then
				return ['name','phone','address','city','state', 'zip','color','time','datetime','datetime-add'].include?(field.template)
			end
		end
		return false
	end
	def generate_field(field,constraint=nil)
		name = field.name
		options = []
		if field.field_options != 'null' then
			options = JSON.parse(field.field_options)
		end
		template = field.template
		#NAMES
		if template == 'name' then
			if options['style'] == 'first' then
				query = FirstNames
				query=query.where(gender:'M') if options['gender'] == 'male'
				query=query.where(gender:'F') if options['gender'] == 'female'
				return query.limit(1).offset(Random.rand(query.count))[0].name
			end
			if options['style'] == 'last' then
				return LastNames.limit(1).offset(Random.rand(LastNames.count))[0].name
			end
			if options['style'] == 'full' then
				query = FirstNames
				query=query.where(gender:'M') if options['gender'] == 'male'
				query=query.where(gender:'F') if options['gender'] == 'female'
				firstName = query.limit(1).offset(Random.rand(query.count))[0].name
				lastName = LastNames.limit(1).offset(Random.rand(LastNames.count))[0].name
				return "#{firstName} #{lastName}"
			end
 		end
		if template == 'phone' then
			return options['format'].gsub(/#/){Random.rand(9)}
		end
		if template == 'address' then
			street = Random.rand(280).ordinalize
			streetType = ['ave','st', 'dr', 'pl', 'cir'].sample
			return "#{street} #{streetType}"
		end
		if template == 'zip' then
			return ZipCodes.limit(1).offset(Random.rand(ZipCodes.count))[0].zip
		end
		if template == 'city' then
			query = ZipCodes
			if constraint != nil then
				query = query.where(state: constraint)
			end
			return query.limit(1).offset(Random.rand(query.count))[0].city

		end
		if template == 'state' then
			return ZipCodes.limit(1).offset(Random.rand(ZipCodes.count))[0].state
		end
		if template == 'color' then
			return Color.limit(1).offset(Random.rand(Color.count))[0].name if options['color_type'] == 'name'
			return Color.limit(1).offset(Random.rand(Color.count))[0].hex if options['color_type'] == 'hex'
		end
		if template == 'number-range' then
			range = options['end'].to_i - options['start'].to_i
			num = Random.rand(range)
			return num + options['start'].to_i
		end
		if template == "number-list" then
			return options['list'].split(',').sample
		end
		if template == 'number-sequential' then
			startAt = options['start'].to_i
			endAt = options['end'].to_i
			step = options['step'].to_i
			val = startAt + step*@generation_count
			return val
		end
		if template == 'float' then
			startAt = options['start'].to_i
			endAt = options['end'].to_i
			return (rand * (endAt-startAt)+startAt).round(options['precision'].to_i)
		end
		if template == 'time' then
			startHour,startMinute = /([0-9]+):([0-9]+)/.match(options['start']).captures
			endHour,endMinute = /([0-9]+):([0-9]+)/.match(options['end']).captures
			hour = (Random.rand(endHour.to_i - startHour.to_i) + startHour.to_i).to_s
			minute = (Random.rand(endMinute.to_i - startMinute.to_i) + startMinute.to_i).to_s
			return "#{hour.rjust(2,'0')}:#{minute.rjust(2,'0')}"
		end
		if template == 'date' || template == 'datetime' then
			startDate = Time.parse(options['start'])
			endDate = Time.parse(options['end'])
      format = options['format']
      randomTime = Time.at(startDate.to_f + Random.rand(endDate.to_f - startDate.to_f))             
      return randomTime.strftime(format)
		end
    if template == 'datetime-add' then
  		min = options['min'].to_i
      max = options['max'].to_i        
      unit = options['unit']
      format = options['format']
      base = Time.parse(constraint)
      base = base + (((max-min) * rand)+min).seconds if unit == 'seconds'
      base = base + (((max-min) * rand)+min).minutes if unit == 'minutes'
      base = base + (((max-min) * rand)+min).hours if unit == 'hours'
      base = base + (((max-min) * rand)+min).days if unit == 'days'
      base = base + (((max-min) * rand)+min).months if unit == 'months'
      base = base + (((max-min) * rand)+min).years if unit == 'years'
      return base.strftime(format)
    end 
    
		return ""
	end
	def output
		guid = params[:guid]
		@dataset = Dataset.where('guid=?', guid).first
		@data = generate_dataset(@dataset, 100)
		if params[:format] == 'mysql'  ||  params[:format] == 'mssql'then
			@data.each do |item|
				item.each {|k,v|item[k]="'" + v + "'" if is_string?(k)}
			end
		end
	end

end
